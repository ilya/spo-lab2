package ru.JNI;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Filesystem  implements AutoCloseable{
    static {
        Filesystem.loadLibrary("lib/linux/NTFS.so");
    }

    public Filesystem(String filepath) throws NoSuchFileException {
        startWork(filepath);
        if (infoStructPtr == 0) throw new NoSuchFileException("Can't load partition on path: " + filepath);
    }

    private long infoStructPtr = 0;

    public static native String getHelpMessage();
    public static native String listAllPartitions();
    public static native String printExitText(String argumentName, String programName);
    public String   ls(){
        ArrayList<ru.JNI.File> result = lsFunc(infoStructPtr);
        StringBuilder builder = new StringBuilder();
        for (ru.JNI.File rs : result){
            if (rs.isDir()){
                builder.append("<DIR>  ");
            } else {
                builder.append("<FILE> ");
            }
            builder.append(rs.getName()).append("\n");
        }
        return builder.toString();
    }
    public ArrayList<ru.JNI.File> lsFiles() {
        return lsFunc(infoStructPtr);
    }
    public String   cd(String path){
        return cdFunc(infoStructPtr, path);
    }
    public String   pwd(){
        return pwdFunc(infoStructPtr);
    }
    public String   cp(String whatToCopy, String path){
        return cpFunc(infoStructPtr, whatToCopy, path);
    }
    public String   help(String command){
        return helpFunc(command.equals("") ? 0 : 1, command); // meh
    }
    public String   exit(){
        long tmp = infoStructPtr;
        infoStructPtr = 0;
        return exitFunc(tmp);
    }

    private void            startWork(String nameOfPartition){
        infoStructPtr = loadPartition(nameOfPartition);
    }
    private native long     loadPartition(String nameOfPartition);
    private native ArrayList<ru.JNI.File> lsFunc(long ptr);
    private native String   cdFunc(long ptr, String path);
    private native String   pwdFunc(long ptr);
    private native String   cpFunc(long ptr, String whatToCopy, String path);
    private native String   helpFunc(int size, String command);
    private native String   exitFunc(long ptr);

    //Helper function to right loading of library
    private static void loadLibrary(String fileName) {
        try {
            URL url = Filesystem.class.getResource("/" + fileName);
            File tmpDir = Files.createTempDirectory("libs").toFile();
            tmpDir.deleteOnExit();
            File nativeLibTmpFile = new File(tmpDir, fileName);
            nativeLibTmpFile.deleteOnExit();
            try (InputStream in = Objects.requireNonNull(url).openStream()) {
                Files.createDirectories(nativeLibTmpFile.toPath().getParent());
                Files.copy(in, nativeLibTmpFile.toPath());
            }
            System.load(nativeLibTmpFile.getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws Exception {
        if (infoStructPtr != 0){
            exitFunc(infoStructPtr);
        }
    }
}
