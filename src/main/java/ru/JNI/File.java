package ru.JNI;

public class File {
    private String name;
    private boolean isDir;

    public File(String name, boolean isDir){
        this.isDir = isDir;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isDir() {
        return isDir;
    }

    @Override
    public String toString() {
        if (isDir)
            return "<DIR>  "+name;
        else
            return "<FILE> "+name;
    }
}
