package ru;

import ru.JNI.File;
import ru.JNI.Filesystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class TreePrinter {

    String collectTree(File node) {
        StringBuilder sb = new StringBuilder();
        collectTreeImpl(sb, "", "", node);
        return sb.toString();
    }

    void collectTreeImpl(StringBuilder sb, String prefix, String childPrefix, File node) {
        sb.append(prefix).append(" ").append(node.getName().replaceAll("\n", "\\\\n").replaceAll("\t", "\\\\t")).append("\n");

        if (!node.isDir()) {
            return;
        }

        if (!filesystem.cd(node.getName()).isEmpty()) {
            return;
        }

        ArrayList<File> subnodes = filesystem.lsFiles();
        if (subnodes == null) {
            filesystem.cd("..");
            return;
        }

        for (int i = 0; i < subnodes.size(); i++) {
            File item = subnodes.get(i);

            if (i < subnodes.size() - 1) {
                collectTreeImpl(sb, childPrefix + " ├──", childPrefix + " │  ", item);
            } else {
                collectTreeImpl(sb, childPrefix + " └──", childPrefix + "    ", item);
            }
        }

        if (subnodes.size() > 0 && subnodes.get(subnodes.size() - 1).isDir()) {
            if (filesystem.cd(subnodes.get(subnodes.size() - 1).getName()).isEmpty()) {
                if (Optional.ofNullable(filesystem.lsFiles()).map(List::isEmpty).orElse(true)) {
                    sb.append(childPrefix).append("\n");
                }

                if (!filesystem.cd("..").isEmpty()) {
                    System.exit(-1);
                }
            }
        }

        if (!filesystem.cd("..").isEmpty()) {
            System.exit(-1);
        }
    }

    private final Filesystem filesystem;
    public TreePrinter(Filesystem system){
        this.filesystem = system;
    }

    public void printTree() {
        //printTree(new File(filesystem.pwd().trim(), ""));
        System.out.println(collectTree(new File(filesystem.pwd().trim(),true)));
    }

    private void printTree(File file, String offset) {
        System.out.printf("%s%s\n", offset, file.getName());

        if (file.isDir()) {
            if (!filesystem.cd(file.getName()).isEmpty()) {
                System.out.println("AAAAAA");
                System.exit(-1);
            }

            List<File> files = filesystem.lsFiles();
            if (files != null) {
                /*for (File f : files) {
                    printTree(f, offset + "    ");
                }*/
            }

            if (!filesystem.cd("..").isEmpty()) {
                System.out.println("AAAAAA");
                System.exit(-1);
            }
        }
    }
}
