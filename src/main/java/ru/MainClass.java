package ru;

import ru.JNI.File;
import ru.JNI.Filesystem;

import java.nio.file.NoSuchFileException;
import java.util.Scanner;

public class MainClass {
    private static final String programName = "lab";

    public static void main(String[] args) {
        if (args.length == 0){
            System.out.println( Filesystem.printExitText("lab",programName));
        }
        switch (args[0]) {
            case "-h":
            case "--help":
                System.out.println(Filesystem.getHelpMessage());
                break;
            case "-w":
            case "--work":
                if (args.length == 1) {
                    System.out.println(Filesystem.printExitText("-w", programName));
                    return;
                }
                MainClass.parseCommands(args[1]);
                return;
            case "-l":
            case "--list":
                System.out.println(Filesystem.listAllPartitions());
                return;
        }
        System.out.println(Filesystem.printExitText(programName, programName));
    }
    public static void parseCommands(String path){
        Filesystem filesystem;

        try {
            filesystem = new Filesystem(path);
        }catch (NoSuchFileException e){
            System.out.println("file "+path + " is not a NTFS file");
            return;
        }
        boolean shouldStop = false;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Partition loaded");
        while (!shouldStop){
            System.out.print("> ");
            String input = scanner.nextLine();
            String[] arr = input.trim().split(" ");

            switch (arr[0]){
                case "help":
                    System.out.println(filesystem.help(arr.length >= 2 ? arr[1] : ""));
                    break;
                case "ls":
                    System.out.println(filesystem.ls());
                    break;
                case "cd":
                    if (arr.length != 2){
                        System.out.println("Insufficient amount of commands. try 'help cd'");
                        break;
                    }
                    String result = filesystem.cd(arr[1]);
                    if (!result.equals("")) System.out.println(result);
                    break;
                case "pwd":
                    System.out.println(filesystem.pwd());
                    break;
                case "cp":
                    if (arr.length != 3){
                        System.out.println("Insufficient amount of commands. try 'help cp'");
                        break;
                    }
                    System.out.println(filesystem.cp(arr[1],arr[2]));
                    break;
                case "exit":
                    System.out.println(filesystem.exit());
                    shouldStop = true;
                    break;
                case "tree":
                    new TreePrinter(filesystem).printTree();
                    break;
                default:
                    if (!arr[0].equals("")) System.out.println("Command not found, try 'help'");
                    break;
            }
        }
    }
}