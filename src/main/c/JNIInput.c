#include <JNI/ru_JNI_Filesystem.h>
#include <stdlib.h>

#include "main.h"
#include "List/list.h"
#include "Work/Handler.h"
#include "Parser/Commands.h"



JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_getHelpMessage
        (JNIEnv *env, jclass class){
}

JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_listAllPartitions
        (JNIEnv *env , jclass class){
    char* rslt = listAllPartitions();
    jstring str = (*env)->NewStringUTF(env, rslt);
    free(rslt);
    return str;
}

JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_printExitText
        (JNIEnv *env, jclass class, jstring argumentName, jstring programName){
    char* argName = (*env)->GetStringUTFChars(env,argumentName,NULL);
    char* progName = (*env)->GetStringUTFChars(env,programName,NULL);
    char* rslt = exitText(argName,progName);
    jstring str = (*env)->NewStringUTF(env, rslt);
    free(rslt);
    return str;
}

JNIEXPORT jlong JNICALL Java_ru_JNI_Filesystem_loadPartition
        (JNIEnv *env, jobject obj, jstring partition){
    char* arg = (*env)->GetStringUTFChars(env,partition,NULL);
    NTFSInfoStruct* result = startWorkReturnInfo(arg);
    return (jlong)result;
}

JNIEXPORT jobject JNICALL Java_ru_JNI_Filesystem_lsFunc
        (JNIEnv *env, jobject obj, jlong structPtr){
    NTFS_ls* currentNative = command_ls((NTFSInfoStruct*)structPtr,0,NULL);
    if (currentNative == NULL) {
        return NULL;
    }

    jclass cls = (*env)->FindClass(env, "ru/JNI/File");
    jmethodID constructor = (*env)->GetMethodID(env,cls, "<init>","(Ljava/lang/String;Z)V");
    jstring str = (*env)->NewStringUTF(env,currentNative->name);
    jobject File = (*env)->NewObject(env,cls, constructor,"",false);

    jclass arrayList_class = (*env)->FindClass(env, "java/util/ArrayList");
    jmethodID arrayList_constructor = (*env)->GetMethodID(env, arrayList_class, "<init>", "()V");
    jobject resultArray = (*env)->NewObject(env, arrayList_class, arrayList_constructor);

    NTFS_ls * orig = currentNative;
    fprintf(stderr, "%d\n", orig->isDir);

    while (currentNative != NULL){
        jstring innerString = (*env)->NewStringUTF(env,currentNative->name);
        jobject createdFile = (*env)->NewObject(env,cls, constructor,innerString,currentNative->isDir);
        jmethodID addMethodToArrayList =
                (*env)->GetMethodID(env, arrayList_class, "add", "(Ljava/lang/Object;)Z");
        (*env)->CallBooleanMethod(env, resultArray, addMethodToArrayList, createdFile);
        NTFS_ls* tmp = currentNative;
        currentNative = currentNative->next;

        free(tmp->name);
        free(tmp);
    }

    return resultArray;
}

JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_cdFunc
        (JNIEnv *env, jobject obj, jlong structPtr, jstring path){
    char* arg = (*env)->GetStringUTFChars(env,path,NULL);
    char* rslt = command_cd((NTFSInfoStruct*)structPtr,1,&arg);
    jstring str = (*env)->NewStringUTF(env, rslt);
    //free(rslt);
    return str;
}

JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_pwdFunc
        (JNIEnv *env, jobject obj, jlong structPtr){
    char* rslt = command_pwd((NTFSInfoStruct*)structPtr,0,NULL);
    jstring str = (*env)->NewStringUTF(env, rslt);
    free(rslt);
    return str;
}

JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_cpFunc
        (JNIEnv *env, jobject obj, jlong structPtr, jstring whatCopy, jstring whereCopy){
    char* args[2];
    args[0] = (*env)->GetStringUTFChars(env,whatCopy,NULL);
    args[1] = (*env)->GetStringUTFChars(env,whereCopy,NULL);
    char* rslt = command_cp((NTFSInfoStruct*)structPtr,2, args);

    jstring str = (*env)->NewStringUTF(env, rslt);
    //free(rslt);
    return str;
}

JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_helpFunc
        (JNIEnv *env, jobject obj, jint size, jstring arg){
    char* input = (*env)->GetStringUTFChars(env,arg,NULL);
    char* args[1];
    args[0] = input;
    char* rslt = command_help(NULL,size,args);
    jstring str = (*env)->NewStringUTF(env, rslt);
    free(rslt);
    return str;
}

JNIEXPORT jstring JNICALL Java_ru_JNI_Filesystem_exitFunc
        (JNIEnv *env, jobject obj, jlong structPtr){
    char* rslt = command_exit((NTFSInfoStruct*)((void*)structPtr),0,NULL);
    jstring str = (*env)->NewStringUTF(env, rslt);
    //free(rslt);
    return str;
}