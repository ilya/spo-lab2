#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "List/list.h"

#define COMMAND "df -h"
#define AMOUT_OF_BYTES 1000
char* listAllPartitions(){
    FILE* f = popen(COMMAND, "r");
    if (f == NULL){
        return "Failed to use command";
    }
    char* output = malloc(AMOUT_OF_BYTES);
    sprintf(output,"Seriously? You can't write "COMMAND" yourself?..\nOk, here it is:\n$: "COMMAND"\n");
    //system(COMMAND);
    char tmpOutput[256];
    while ((fgets(tmpOutput,256,f))!= NULL){
        strcat(output,tmpOutput);
    }
    pclose(f);
    return output;
}
