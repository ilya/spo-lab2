#include "Commands.h"

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "work/NTFS.h"
#include "work/InnerFunctions.h"



run_command RUN_COMMANDS_LIST[] ={
        {
            .name = "exit",
            .description = "Close partition and exit program, no arguments",
            .function = command_exit
        }, {
            .name = "help",
            .description = "shows short help about commands, 1 argument - shows longer description",
            .function = command_help
        }, {
            .name = "ls",
            .description = "list files and directories. With path, shows files and directories in it",
            .function = command_ls
        }, {
            .name = "cp",
            .description = "Copy files, 2 arguments - from to where you want to copy it",
            .function = command_cp
        }, {
            .name = "pwd",
            .description = "Shows current working folder",
            .function = command_pwd
        }, {
            .name = "cd",
            .description = "change folder, 1 argument - to what directory change",
            .function = command_cd
        }
};


int NUMBER_OF_COMMANDS = sizeof (RUN_COMMANDS_LIST)/sizeof (RUN_COMMANDS_LIST[0]);

char* command_exit(  NTFSInfoStruct* ntfs, int size, char** args){
    freeInode(ntfs->root_inode);
    free(ntfs);
    return "Partition closed";
}
char* command_help(  NTFSInfoStruct* ntfs, int size, char** args){
    char* buffer = malloc(700);
    if (size > 0){
        for (int i = 0; i< NUMBER_OF_COMMANDS; i++){
            if(memcmp(RUN_COMMANDS_LIST[i].name,args[0], strlen(args[0]))==0){
                sprintf(buffer,"%s:\n%s\n",RUN_COMMANDS_LIST[i].name,
                                    RUN_COMMANDS_LIST[i].description);
                return buffer;
            }
        }
        return ("No matching commands found, try 'help'");
    }
    char tmpBuff[265];
    for (int i = 0; i< NUMBER_OF_COMMANDS; i++) {
        sprintf(tmpBuff,"%s - %s\n",RUN_COMMANDS_LIST[i].name,RUN_COMMANDS_LIST[i].description);
        strcat(buffer,tmpBuff);
    }

    return buffer;
}
char* command_cp(    NTFSInfoStruct* ntfs, int size, char** args){
    if (size < 1){
        return ("Unspecified what to copy and where. Abort");
    }
    if (size <2){
        return ("Unspecified where to copy. Abort");
    }

    ntfs_inode* inputInode = ntfs->current_inode;
    NTFSFindInfo * result;
    int err = findNodeByName(ntfs,args[0], &inputInode, &result);
    if (err == -1){
        return ("No such file or directory. Abort");
    }
    err = copyByInode(ntfs,result->result, args[1]);
    if (err == -1){
        return ("Some error occurred");
    }
    return ("Copied");
}

#define DIR_NAME "<DIR>"
#define FILE_NAME "<FILE>"
NTFS_ls* command_ls(    NTFSInfoStruct* ntfs, int size, char** args){
    NTFSFindInfo* result_info = malloc(sizeof(NTFSFindInfo));
    result_info->result = ntfs->current_inode;

    int err = ntfsReadDir(ntfs, &(result_info->result));
    if (err == -1 )
        return NULL;

    ntfs_inode* tmp_inode =result_info->result->next;
    if (tmp_inode == NULL) {
        return NULL;
    }

    //char tmpResult[265];
    //char* output = malloc(265 * err);
    //output[0] = '\0';
    NTFS_ls* ls = malloc(sizeof(NTFS_ls));
    NTFS_ls* current = ls;
    while (tmp_inode != NULL){
        fprintf(stderr, "filename is %s",tmp_inode->filename);
        current->name = strdup(tmp_inode->filename);
        if (tmp_inode->type & MFT_RECORD_IS_DIRECTORY){
            current->isDir = true;
            //sprintf(tmpResult,"%-8s%s\n",DIR_NAME,tmp_inode->filename);
        } else {
            //sprintf(tmpResult,"%-8s%s\n",FILE_NAME,tmp_inode->filename);
            current->isDir = false;
        }
        //strcat(output,tmpResult);
        tmp_inode = tmp_inode->next;

        if (tmp_inode != NULL) {
            current->next = malloc(sizeof(NTFS_ls));
            current = current->next;
        }
    }

    current->next = NULL;

    freeInode(result_info->result->next);
    result_info->result->next = NULL;

    result_info->result = NULL;
    result_info->start = NULL;

    free(result_info);

    return ls;
}
char* command_pwd(   NTFSInfoStruct* ntfs, int size, char** args){
    ntfs_inode* temp = ntfs->root_inode->next;
    size_t maxSize = 265;
    size_t currentSize = 1;
    size_t nameSize = 0;
    char* output = malloc(maxSize);
    output[0] = '\0';
    strcat(output, "/");
    while (temp != NULL){
        nameSize = strlen(temp->filename);
        if (nameSize+ currentSize + 1 > maxSize){
            output = realloc(output, currentSize+nameSize+1);
            maxSize = currentSize+nameSize+1;
        }
        strcat(output,temp->filename);
        temp = temp->next;
        if (temp != NULL)
            strcat(output,"/");
        currentSize +=nameSize+1;
    }
    strcat(output, "\n");
    return output;
}
char* command_cd(    NTFSInfoStruct* ntfs, int size, char** args){
    if (size < 1 || strcmp(args[0], ".") == 0)
        return "";

    if (strcmp(args[0], "..")==0){
        if (ntfs->current_inode->mft_number == FILE_root){
            return "";
        }
        ntfs_inode* tmp =ntfs->current_inode->parent;
        freeInode(ntfs->current_inode);
        ntfs->current_inode =tmp;
        ntfs->current_inode->next = NULL;
        return "";
    }



    NTFSFindInfo* result;
    if (args[0][0] == '/' && args[0][1] == '\0'){
        freeInode(ntfs->root_inode->next);
        ntfs->current_inode = ntfs->root_inode;
        return "";
    } else if (args[0][0] == '/'){
        int error = findNodeByName(ntfs, args[0], &(ntfs->root_inode), &result);
        if (error == -1) goto nothing;
        if (result->result->type & MFT_RECORD_IS_DIRECTORY){
            ntfs->root_inode->next = result->start->next;
            result->start->next->parent = ntfs->root_inode;
            ntfs->current_inode = result->result;
            result->start->next = NULL;
            freeInode(result->start);
            free(result);
            return "";
        }
    }
    else {
        int error = findNodeByName(ntfs, args[0], &(ntfs->current_inode), &result);

        if (error == -1) {
            goto nothing;
        }

        if (result->result->type & MFT_RECORD_IS_DIRECTORY) {
            ntfs->current_inode->next = result->start->next;
            result->start->next->parent = ntfs->current_inode;
            ntfs->current_inode = result->result;
            result->start->next = NULL;
            freeInode((result->start));
            free(result);
            return "";
        } else { //If it's a file
            freeInode(result->start);
            free(result);
            return ("Not a directory");
        }
    }
    nothing:
        return ("No such file or directory");
}