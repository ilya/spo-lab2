#pragma once
#include <stdbool.h>
#include <work/Handler.h>

typedef struct {
    char* name;
    char* description;
    void* (*function)(NTFSInfoStruct*, int, char**);
}run_command;
typedef struct NTFS_ls NTFS_ls;
struct NTFS_ls {
    char* name;
    bool isDir;
    NTFS_ls* next;
};

extern run_command RUN_COMMANDS_LIST[];
extern int NUMBER_OF_COMMANDS;

char* command_exit   (NTFSInfoStruct*, int,   char**);
char* command_help   (NTFSInfoStruct*, int,   char**);
NTFS_ls* command_ls     (NTFSInfoStruct*, int,   char**);
char* command_cp     (NTFSInfoStruct*, int,   char**);
char* command_pwd    (NTFSInfoStruct*, int,   char**);
char* command_cd     (NTFSInfoStruct*, int,   char**);
